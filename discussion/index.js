// Operators
// Add + = SUM
	// Subtract - = Difference
	// Multiply * = Product
	// Divide / = Quotient
	// Modulus % = Remainder
// 

function mod(){
	return 9 % 2;
}

console.log(mod());


// 

let sum = 1;
sum +=1;

console.log(sum);

let z = 1;

let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

let juan = 'juan';

console.log(1 == 1);
console.log(0 == false);
console.log('juan' == juan);

// strict equality (===) 
console.log(1 === true);

//Inequality Operator
console.log("Inequality Operator");
console.log(1 != 1);
console.log('Juan' != juan)

// Strict Inequality
console.log(0 !== false)


// Other comparison operators

// > , < , >= , <=


// Logical Operators

// and operator (&&) - returns true if all operands are true 
// true && true = true
// false && true = false
// true && false = false
// false && false = false

// or operator

/*

Or Operator (||) - returns true if at least one operand is true
// true && true = true
// false && true = true
// true && false = true
// false && false = false

*/


let isLegalAge = true;
let isRegistered = false;


let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet)

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

  // Selection Control Structures

  /*
	IF statement 
	-execute a statement if a specified condition is true

	Syntax
	if(condition){
		statement/s
	}
	else{
		statement/s
	}

  */ 

let num = -1;

if(num < 0){
	console.log('Hello');
}

let num2 = 8;

if(num2 >= 10){
	console.log('Welcome to Zuitt');
}

else{
	console.log('GoodBye');
}

num = 5;
if(num >10){
	console.log("Number is greater or equal to 10");
}
else{
	console.log("Number is not greater or equal to 10")
}

// let age = parseInt(prompt("Please enter age: "));

// if(age > 59){
// 	alert("Senior Age");
// }
// else{
// 	alert("Invalid Age");
// }

// let city = parseInt(prompt("Enter a number: "));

// else if (city === 1){
// 	alert("welcome to quezon city");
// }


// else if (city === 2){
// 	alert("welcome to pasig city");
// }

// else if (city === 3){
// 	alert("welcome to la union city");
// }

// else{
// 	alert("invalid");
// }


function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return 'Not a typhoon yet';
	}
	else if(windSpeed <= 61){
		return 'Tropical depression detected';
	}
	else if(windSpeed >= 61 && windSpeed <= 88){
		return 'Tropical storm detected.';
	}
	else if(windSpeed >= 89 && windSpeed <=117){
		return'Severe tropical storm detected';
	}
	else{
		return 'Typhoon detected';
	}
}

message = determineTyphoonIntensity(70);
console.log(message);


// Ternary Operator
/*
	Syntax:
	(condition) ? ifTrue : ifFalse

*/
let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

let name;

function isOfLegalAge(){
	name = 'John';
	return 'You are of the legal age limit';
}

function isUnderAge(){
	name = 'Jane';
	return 'You are under the age limit';
}

// let age = parseInt(prompt("What is your Age"));
// let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
// alert("Result of Ternary Operator in functions: " + legalAge + ', ' + name);

// Switch statement
/* Syntax:
	switch (expression){
	case value1:
		statement/s;
		break
	case value2:
		statement/s;
		break
	case valueN:
		statement/s;
		break
	default:
		statement/s;
	}
	
*/

let day = prompt("What day of the week is it today?").toLowerCase();

switch (day){
	case 'sunday':
		alert("The color of the day is red");
		break;
	case 'monday':
		alert("The color of the day is orange");
		break;
	case 'tuesday':
		alert("The color of the day is yellow");
		break;
	case 'wednesday':
		alert("The color of the day is green");
		break;
	case 'thursday':
		alert("The color of the day is blue");
		break;
	case 'friday':
		alert("The color of the day is black");
		break;
	case 'saturday':
		alert("The color of the day is violet");
		break;
	default:
		alert("Please input valid day");
}

function showIntensityAlert(windSpeed){
	try{
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch (error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert('Intensity updates will show new alert')
	}
}

showIntensityAlert(56);





